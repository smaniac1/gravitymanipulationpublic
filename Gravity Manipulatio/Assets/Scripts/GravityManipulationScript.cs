﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityManipulationScript : MonoBehaviour
{

    public string direction;
    public float bulletSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            GetComponent<AudioSource>().Play();
            //Based on what direction you want, send the bullet in said directon
            if (direction == "x+")
            {
                collision.GetComponent<Rigidbody>().velocity = Vector3.zero;
                collision.GetComponent<Rigidbody>().AddForce(Vector3.right * bulletSpeed);
                collision.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
            }
            else if(direction == "x-")
            {
                collision.GetComponent<Rigidbody>().velocity = Vector3.zero;
                collision.GetComponent<Rigidbody>().AddForce(Vector3.left * bulletSpeed);
                collision.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
            }
            else if(direction == "y+")
            {
                collision.GetComponent<Rigidbody>().velocity = Vector3.zero;
                collision.GetComponent<Rigidbody>().AddForce(Vector3.up * bulletSpeed);
                collision.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            }
            else if(direction == "y-")
            {
                collision.GetComponent<Rigidbody>().velocity = Vector3.zero;
                collision.GetComponent<Rigidbody>().AddForce(Vector3.down * bulletSpeed);
                collision.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            }
            else if(direction == "z+")
            {
                collision.GetComponent<Rigidbody>().velocity = Vector3.zero;
                collision.GetComponent<Rigidbody>().AddForce(Vector3.forward * bulletSpeed);
                collision.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY;
            }
            else if(direction == "z-")
            {
                collision.GetComponent<Rigidbody>().velocity = Vector3.zero;
                collision.GetComponent<Rigidbody>().AddForce(Vector3.back * bulletSpeed);
                collision.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY;
            }
            else
            {
                //Do nothing if no instructions are put in direction
            }
        }
        
    }
}
