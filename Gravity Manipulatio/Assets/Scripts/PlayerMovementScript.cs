﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour
{

    public CharacterController controller;
    public Canvas instructionMenu;
    public float jumpspeed = 2;
    public float speed = 12f;

    private float ySpeed;
    private bool iMenu = true;
    private float originalStepOffset;

    void Start()
    {
        originalStepOffset = controller.stepOffset;
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = (transform.right * x) * speed + (transform.forward * z) * speed;
        move.y = ySpeed;

        controller.Move(move * Time.deltaTime);

        ySpeed += Physics.gravity.y * Time.deltaTime;
        if (controller.isGrounded)
        {
            controller.stepOffset = originalStepOffset;
            ySpeed = -0.5f;
            if (Input.GetButtonDown("Jump"))
            {
                ySpeed = jumpspeed;
            }
        }
        else
        {
            controller.stepOffset = 0;
        }
        if (Input.GetButtonDown("Instructions"))
        {
            iMenu = !iMenu;
            instructionMenu.gameObject.SetActive(iMenu);
        }

    }
}
