﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private LayerMask layerMask;

    //Variables for manipulators
    public GameObject Xp;
    public GameObject Xm;
    public GameObject Zp;
    public GameObject Zm;


    //Variable used to ditermine which manipulator is up
    private int direction = 0;

    private void Update()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out RaycastHit raycastHit, float.MaxValue, layerMask)){
            transform.position = raycastHit.point;
        }

        //Based on the key set the correct game object to true;
        if(Input.GetKeyDown(KeyCode.D) && direction != 0)
        {
            direction = 0;
            Xp.SetActive(true);
            Xm.SetActive(false);
            Zp.SetActive(false);
            Zm.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.S) && direction != 1)
        {
            direction = 1;
            Xp.SetActive(false);
            Xm.SetActive(false);
            Zp.SetActive(false);
            Zm.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.A) && direction != 2)
        {
            direction = 2;
            Xp.SetActive(false);
            Xm.SetActive(true);
            Zp.SetActive(false);
            Zm.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.W) && direction != 3)
        {
            direction = 3;
            Xp.SetActive(false);
            Xm.SetActive(false);
            Zp.SetActive(true);
            Zm.SetActive(false);
        }
    }
}
