﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private float timeAlive;
    public float LifeSpan;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeAlive += Time.deltaTime;
        if (timeAlive >= LifeSpan)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Enviroment")
        {
            Destroy(gameObject);
        }
        if(collision.gameObject.tag == "Target")
        {
            collision.gameObject.GetComponent<AudioSource>().Play();
            Destroy(collision.gameObject, 1.5f);
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Dummy")
        {
            collision.gameObject.GetComponent<AudioSource>().Play();
            Destroy(gameObject);
        }
    }
}
