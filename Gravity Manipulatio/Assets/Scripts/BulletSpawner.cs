﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    public Transform Bullet;
    public float BulletSpeed;
    public float spawnTimer;
    private float timeSinceSpawn = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceSpawn += Time.deltaTime;
        if(timeSinceSpawn >= spawnTimer)
        {
            timeSinceSpawn = 0;
            Transform BulletTrans = Instantiate(Bullet, transform.position, transform.rotation);
            Rigidbody BulletRB = BulletTrans.GetComponent<Rigidbody>();
            BulletRB.AddRelativeForce(Vector3.forward * BulletSpeed);
        }
    }
}
