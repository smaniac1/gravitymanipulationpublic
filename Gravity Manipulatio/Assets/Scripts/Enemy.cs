﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float sightRange;
    public float turnSpeed;

    private bool bulletInSight;

    private Transform target;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, .1f);
    }

    void UpdateTarget()
    {
        GameObject[] bullets = GameObject.FindGameObjectsWithTag("Bullet");
        float shortestDistance = Mathf.Infinity;
        GameObject nearestBullet = null;

        foreach (GameObject bullet in bullets)
        {
            float distanceToBullet = Vector3.Distance(transform.position, bullet.transform.position);
            if (distanceToBullet < shortestDistance)
            {
                shortestDistance = distanceToBullet;
                nearestBullet = bullet;
            }
        }

        if (nearestBullet != null && shortestDistance <= sightRange)
        {
            target = nearestBullet.transform;
        }
        else
        {
            target = null;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            defend();
        }
    }

    private void defend()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }
}
